/* 
    Section while loop 
    
    Iteration is the term given to the repetition of statements
*/

// console.log('hello');

// let count = 5;

// while(count !== 0) {
//    console.log(`while: ${count}`);

//    count--;
// }


/* 
   
    Section Do While loop
    
*/


// let number = Number(prompt("Give a Number"));

// do {

//     // The current value of number
//     console.log("Do While: " + number); 

//     number += 1;
// } while(number <= 10)


/* 
  
   section For loops

*/


// for(let count = 0; count <= 28; count++) {
//     console.log(count);
// }


// let myString = 'Alex';

// console.log(myString.length);
// console.log(myString[0]);
// console.log(myString[1]);
// console.log(myString[2]);



// for(let x = 0; x < myString.length; x++) {
//     console.log(myString[x]);
// }



// Create a string name

let myName = "Reynan Delizo";

for(let i = 0; i < myName.length; i++) {
    // console.log(myName[i].toLowerCase());

    if(
       myName[i].toLowerCase() == "a" ||
       myName[i].toLowerCase() == "e" ||
       myName[i].toLowerCase() == "i" ||
       myName[i].toLowerCase() == "o" ||
       myName[i].toLowerCase() == "u"  
    ){
       console.log(3); 
    } else {
        console.log(myName[i])
    }
}



for(let count = 0; count <= 20; count++) {
    if(count % 2 === 0){
        continue;
    }

    console.log(` Continue and break ${count} `);

    if(count > 10) {
        break;
    }
}

let name = "alexandro";

for(let i = 0; i <= name.length; i++) {
    console.log(name[i]);

    if(name[i].toLowerCase() === "a" ){
        console.log("Continue to the next iteration");
        continue;
    }

    if(name[i] == "d") {
        break;
    }
}

